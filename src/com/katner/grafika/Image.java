package com.katner.grafika;

import java.awt.image.BufferedImage;

/**
 * Created by michal on 24.02.16.
 */
public class Image {
    String type = null;
    Integer width = null;
    Integer height = null;
    Integer maxValue = null;
    BufferedImage contents = null;
    Float loadingTime = null;

    public boolean isInfoComplete() {
        return (type != null &&
                width != null &&
                height != null &&
                maxValue != null);
    }

    public boolean hasTypeInfo() {
        return (type != null);
    }

    public void addInfo(int i) {
        if (width == null) {
            System.out.println("writing width");
            width = new Integer(i);
        } else
        if (height == null)
        {
            System.out.println("writing height");
            height = new Integer(i);
        } else
        if (maxValue == null)
        {
            System.out.println("writing maxvalue");
            maxValue = new Integer(i);
        }
    }
}
