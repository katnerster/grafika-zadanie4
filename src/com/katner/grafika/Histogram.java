package com.katner.grafika;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by michal on 25.03.16.
 */
public class Histogram {
    public static ArrayList<int[]> imageHistogram(BufferedImage input) {

        int[] rhistogram = new int[256];
        int[] ghistogram = new int[256];
        int[] bhistogram = new int[256];

        for (int i = 0; i < rhistogram.length; i++) rhistogram[i] = 0;
        for (int i = 0; i < ghistogram.length; i++) ghistogram[i] = 0;
        for (int i = 0; i < bhistogram.length; i++) bhistogram[i] = 0;

        for (int i = 0; i < input.getWidth(); i++) {
            for (int j = 0; j < input.getHeight(); j++) {

                Color color = new Color(input.getRGB(i, j));
                int red = color.getRed();
                int green = color.getGreen();
                int blue = color.getBlue();

                // Increase the values of colors
                rhistogram[red]++;
                ghistogram[green]++;
                bhistogram[blue]++;

            }
        }

        ArrayList<int[]> hist = new ArrayList<int[]>();
        hist.add(rhistogram);
        hist.add(ghistogram);
        hist.add(bhistogram);

        return hist;

    }

    private static ArrayList<int[]> histogramEqualizationLUT(BufferedImage input) {

        // Get an image histogram - calculated values by R, G, B channels
        ArrayList<int[]> imageHist = imageHistogram(input);

        // Create the lookup table
        ArrayList<int[]> imageLUT = new ArrayList<int[]>();

        // Fill the lookup table
        int[] rhistogram = new int[256];
        int[] ghistogram = new int[256];
        int[] bhistogram = new int[256];

        for (int i = 0; i < rhistogram.length; i++) rhistogram[i] = 0;
        for (int i = 0; i < ghistogram.length; i++) ghistogram[i] = 0;
        for (int i = 0; i < bhistogram.length; i++) bhistogram[i] = 0;

        long sumr = 0;
        long sumg = 0;
        long sumb = 0;

        // Calculate the scale factor
        float scale_factor = (float) (255.0 / (input.getWidth() * input.getHeight()));

        for (int i = 0; i < rhistogram.length; i++) {
            sumr += imageHist.get(0)[i];
            int valr = (int) (sumr * scale_factor);
            if (valr > 255) {
                rhistogram[i] = 255;
            } else rhistogram[i] = valr;

            sumg += imageHist.get(1)[i];
            int valg = (int) (sumg * scale_factor);
            if (valg > 255) {
                ghistogram[i] = 255;
            } else ghistogram[i] = valg;

            sumb += imageHist.get(2)[i];
            int valb = (int) (sumb * scale_factor);
            if (valb > 255) {
                bhistogram[i] = 255;
            } else bhistogram[i] = valb;
        }

        imageLUT.add(rhistogram);
        imageLUT.add(ghistogram);
        imageLUT.add(bhistogram);

        return imageLUT;

    }

    public static BufferedImage histogramOperation(BufferedImage original, OPERATION_TYPE type) {

        int red;
        int green;
        int blue;
        int newPixel = 0;

        // Get the Lookup table for histogram equalization
        ArrayList<int[]> histLUT = null;
        switch (type) {
            case HISTOGRAM_EQUALIZATION:
                histLUT = histogramEqualizationLUT(original);
                break;
            case HISTOGRAM_STRECH:
                histLUT = histogramStrechLUT(original);
                break;
        }

        BufferedImage histogramEQ = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

        for (int i = 0; i < original.getWidth(); i++) {
            for (int j = 0; j < original.getHeight(); j++) {
                red = new Color(original.getRGB(i, j)).getRed();
                green = new Color(original.getRGB(i, j)).getGreen();
                blue = new Color(original.getRGB(i, j)).getBlue();

                red = histLUT.get(0)[red];
                green = histLUT.get(1)[green];
                blue = histLUT.get(2)[blue];

                newPixel = (red << 16) + (green << 8) + blue;
                histogramEQ.setRGB(i, j, newPixel);

            }
        }

        return histogramEQ;

    }

    private static ArrayList<int[]> histogramStrechLUT(BufferedImage input) {
        // Get an image histogram - calculated values by R, G, B channels
        ArrayList<int[]> imageHist = imageHistogram(input);
        // Create the lookup table
        ArrayList<int[]> imageLUT = new ArrayList<int[]>();
        int[] dataBuffInt = input.getRGB(0, 0, input.getWidth(), input.getHeight(), null, 0, input.getWidth());

        int rMax = Arrays.stream(dataBuffInt).map(i -> (i >> 16) & 0xFF).max().getAsInt();
        int rMin = Arrays.stream(dataBuffInt).map(i -> (i >> 16) & 0xFF).min().getAsInt();
        int gMax = Arrays.stream(dataBuffInt).map(i -> (i >> 8) & 0xFF).max().getAsInt();
        int gMin = Arrays.stream(dataBuffInt).map(i -> (i >> 8) & 0xFF).min().getAsInt();
        int bMax = Arrays.stream(dataBuffInt).map(i -> i & 0xFF).max().getAsInt();
        int bMin = Arrays.stream(dataBuffInt).map(i -> i & 0xFF).min().getAsInt();

        int[] rhistogram = new int[256];
        int[] ghistogram = new int[256];
        int[] bhistogram = new int[256];

        for (int i = 0; i < 256; i++)
            rhistogram[i] = calculateLUTvalue(rMax, rMin, i);
        for (int i = 0; i < 256; i++)
            ghistogram[i] = calculateLUTvalue(gMax, gMin, i);
        for (int i = 0; i < 256; i++)
            bhistogram[i] = calculateLUTvalue(bMax, bMin, i);

        imageLUT.add(rhistogram);
        imageLUT.add(ghistogram);
        imageLUT.add(bhistogram);
        return imageLUT;
    }

    private static int calculateLUTvalue(int max, int min, int i) {
        if ((255.0 / (max - min) * (i + (-min))) > 255) {
            return 255;
        } else if ((255.0 / (max - min) * (i + (-min))) < 0) {
            return 0;
        } else
            return (int) (255.0 / (max - min) * (i + (-min)));
    }

    public enum OPERATION_TYPE {
        HISTOGRAM_STRECH,
        HISTOGRAM_EQUALIZATION
    }
}
