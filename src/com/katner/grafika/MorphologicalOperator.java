package com.katner.grafika;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;

/**
 * @author katnerm
 */
public class MorphologicalOperator {

    public static boolean[][] erode(boolean[][] input, Item[][] mask) {
        boolean[][] result = Util.createArray(input);
        for (int y = mask.length / 2; y < result.length - (mask.length / 2); y++) {
            for (int x = mask[0].length / 2; x < result[0].length - (mask[0].length / 2); x++) {
                result[y][x] = match(input, mask, y, x);
            }
        }
        return result;
    }

    private static boolean match(boolean[][] input, Item[][] mask, int y, int x) {
        int startX = x - mask[0].length / 2;
        int startY = y - mask.length / 2;

        for (int xx = startX; xx < startX + mask[0].length; xx++) {
            for (int yy = startY; yy < startY + mask.length; yy++) {
                if (mask[yy - startY][xx - startX] == Item.FOREGROUND) {
                    if (input[yy][xx] == false) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean[][] dilate(boolean[][] input, Item[][] mask) {
        boolean[][] result = Util.createArray(input);
        for (int y = mask.length / 2; y < result.length - (mask.length / 2); y++) {
            for (int x = mask[0].length / 2; x < result[0].length - (mask[0].length / 2); x++) {
                if (input[y][x] == true) {
                    apply(result, mask, y, x);
                }
            }
        }
        return result;
    }

    private static void apply(boolean[][] result, Item[][] mask, int y, int x) {
        int startX = x - mask[0].length / 2;
        int startY = y - mask.length / 2;

        for (int xx = startX; xx < startX + mask[0].length; xx++) {
            for (int yy = startY; yy < startY + mask.length; yy++) {
                if (mask[yy - startY][xx - startX] == Item.FOREGROUND) {
                    result[yy][xx] = true;
                }
            }
        }
    }

    static boolean[][] open(boolean[][] input, Item[][] mask) {
        return dilate(erode(input, mask), mask);
    }

    static boolean[][] close(boolean[][] input, Item[][] mask) {
        return erode(dilate(input, mask), mask);
    }

    static boolean[][] hitmiss(boolean[][] input, Item[][][] masks) {
        boolean[][] result = Util.createArray(input);
        for (Item[][] mask : masks) {
            boolean[][] tmp = Util.product(erode(input, mask), erode(Util.complement(input), Util.invertMask(mask)));
            result = Util.sum(result, tmp);
        }
        return result;
    }

    public static boolean[][] thin(boolean[][] input, Item[][][] masks) {
        boolean[][] prev = input.clone();
        boolean[][] next = input.clone();
        do {
            for (Item[][] mask : masks) {
                prev = next;
                next = Util.subtraction(prev, hitmiss(prev, new Item[][][]{mask}));
                if (!Arrays.deepEquals(prev, next)) {
                    break;
                }
            }
        } while (!Arrays.deepEquals(prev, next));
        return next;
    }

    static boolean[][] thick(boolean[][] input, Item[][][] masks) {
        return Util.complement(thin(Util.complement(input), masks));
    }
}
