package com.katner.grafika;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by michal on 24.03.16.
 */
public class MaskDialog {
    public static float[][] show(int rows, int cols) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(rows, cols, 5, 5));
        JFormattedTextField[][] fields = new JFormattedTextField[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                NumberFormat format = DecimalFormat.getInstance();
                NumberFormatter formatter = new NumberFormatter(format);
                formatter.setValueClass(Float.class);
                formatter.setMinimum(Float.MIN_VALUE);
                formatter.setMaximum(Float.MAX_VALUE);
                formatter.setCommitsOnValidEdit(true);
                fields[row][col] = new JFormattedTextField(new DecimalFormat("#,##0.00;-#"));
                panel.add(fields[row][col]);
            }
        }
        JOptionPane.showConfirmDialog(null, panel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        float[][] result = new float[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (fields[row][col].getValue() != null)
                    result[row][col] = ((Number) fields[row][col].getValue()).floatValue();
                else
                    result[row][col] = 0;
            }
        }
        return result;
    }

    public static int[][] showInt(int rows, int cols) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(rows, cols, 5, 5));
        JFormattedTextField[][] fields = new JFormattedTextField[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                NumberFormat format = NumberFormat.getInstance();
                NumberFormatter formatter = new NumberFormatter(format);
                formatter.setValueClass(Integer.class);
                formatter.setMinimum(Integer.MIN_VALUE);
                formatter.setMaximum(Integer.MAX_VALUE);
                formatter.setCommitsOnValidEdit(true);
                fields[row][col] = new JFormattedTextField(formatter);
                panel.add(fields[row][col]);
            }
        }
        JOptionPane.showConfirmDialog(null, panel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        int[][] result = new int[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (fields[row][col].getValue() != null)
                    result[row][col] = ((Number) fields[row][col].getValue()).intValue();
                else
                    result[row][col] = 0;
            }
        }
        return result;
    }
}
