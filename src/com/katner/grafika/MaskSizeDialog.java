package com.katner.grafika;

import javax.swing.*;
import java.awt.*;

/**
 * Created by michal on 24.03.16.
 */
public class MaskSizeDialog {
    public static int[] show() {
        JPanel panel = new JPanel();
        SpinnerNumberModel rowModel = new SpinnerNumberModel(3, 1, 50, 1);
        JSpinner rowsSpinner = new JSpinner(rowModel);
        SpinnerNumberModel colModel = new SpinnerNumberModel(3, 1, 50, 1);
        JSpinner colsSpinner = new JSpinner(colModel);
        panel.setLayout(new GridLayout(2, 2, 5, 5));
        panel.add(new JLabel("Wierszy:"));
        panel.add(rowsSpinner);
        panel.add(new JLabel("Kolumn:"));
        panel.add(colsSpinner);
        JOptionPane.showConfirmDialog(null, panel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        return new int[]{(Integer) rowsSpinner.getValue(), (Integer) colsSpinner.getValue()};
    }

}
