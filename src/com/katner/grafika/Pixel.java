package com.katner.grafika;

/**
 * Created by michal on 27.02.16.
 */
public class Pixel {
    int r, g, b;

    public int toColor() {
        return ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
    }
}
