package com.katner.grafika;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by michal on 23.03.16.
 */
public class Convolution {

    private static int normalize(int input) {
        if (input > 255) {
            return 255;
        } else if (input < 0) {
            return 0;
        } else {
            return input;
        }
    }

    private static int applyMask(BufferedImage source, int x, int y, int[][] mask) {
        BufferedImage subImage = source.getSubimage(x - mask.length / 2, y - mask[0].length / 2, mask.length, mask[0].length);
        int sRed = 0;
        int sGreen = 0;
        int sBlue = 0;
        for (int forx = 0; forx < mask.length; forx++) {
            for (int fory = 0; fory < mask[0].length; fory++) {
                Color color = new Color(subImage.getRGB(forx, fory));
                sRed += mask[forx][fory] * color.getRed();
                sGreen += mask[forx][fory] * color.getGreen();
                sBlue += mask[forx][fory] * color.getBlue();
            }
        }
        int sum = Arrays.stream(mask).mapToInt(ints ->
                IntStream.of(ints).sum()).sum();
        if (sum != 0) {
            sRed /= sum;
            sGreen /= sum;
            sBlue /= sum;
        }
        sRed = normalize(sRed);
        sGreen = normalize(sGreen);
        sBlue = normalize(sBlue);

        return (sRed << 16) + (sGreen << 8) + sBlue;
    }

    public static BufferedImage conv(BufferedImage source, int[][] mask) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics tmpGraphics = result.getGraphics();
        tmpGraphics.drawImage(source, 0, 0, null);
        tmpGraphics.dispose();
        for (int x = mask.length / 2; x < source.getWidth() - mask.length / 2; x++) {
            for (int y = mask[0].length; y < source.getHeight() - mask[0].length / 2; y++) {
                result.setRGB(x, y, applyMask(source, x, y, mask));
            }
        }
        return result;
    }
}
