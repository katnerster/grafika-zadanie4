package com.katner.grafika;

import javax.swing.*;
import java.awt.*;

/**
 * Created by michal on 09.04.16.
 */
public class AngleInputDialog {
    public static int showDialog() {
        JPanel panel = new JPanel();
        SpinnerNumberModel rowModel = new SpinnerNumberModel(0, 0, 359, 1);
        JSpinner rowsSpinner = new JSpinner(rowModel);
        panel.setLayout(new GridLayout(2, 2, 5, 5));
        panel.add(new JLabel("Kąt(w stopniach):"));
        panel.add(rowsSpinner);
        JOptionPane.showConfirmDialog(null, panel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        return (Integer) rowsSpinner.getValue();
    }
}
