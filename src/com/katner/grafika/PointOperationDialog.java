/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katner.grafika;

import javax.swing.*;
import java.util.Arrays;
import java.util.Hashtable;

/**
 * @author katnerm
 */
public class PointOperationDialog {

    private static final String[] OTHER_OPERATION_STRINGS = new String[]{"Czerwony", "Zielony", "Niebieski"};
    private static final String[] LIGHT_CHANGE_STRINGS = new String[]{"Współczynnik zmiany"};
    public int[] returnValues;

    public void showDialog(OPERATION_TYPE type) {
        JPanel dialogPanel = new JPanel();
        dialogPanel.setLayout(new BoxLayout(dialogPanel, BoxLayout.Y_AXIS));
        JSlider[] sliders = null;
        int minValue = (type == OPERATION_TYPE.OPERATION_MULTIPLICATION
                || type == OPERATION_TYPE.OPERATION_DIVISION) ? 10 : (type == OPERATION_TYPE.OPERATION_LIGHT_CHANGE) ? -100 : 0;
        int maxValue = (type == OPERATION_TYPE.OPERATION_MULTIPLICATION
                || type == OPERATION_TYPE.OPERATION_DIVISION
                || type == OPERATION_TYPE.OPERATION_LIGHT_CHANGE
                || type == OPERATION_TYPE.OPERATION_PERCENT_BLACK_SELECTION_BINARIZATION) ? 100 : 255;
        int numOfSliders = (type == OPERATION_TYPE.OPERATION_LIGHT_CHANGE
                || type == OPERATION_TYPE.OPERATION_CUSTOM_BINARIZATION
                || type == OPERATION_TYPE.OPERATION_PERCENT_BLACK_SELECTION_BINARIZATION) ? 1 : 3;
        int initialValue = (type == OPERATION_TYPE.OPERATION_MULTIPLICATION || type == OPERATION_TYPE.OPERATION_DIVISION) ? 10 : 0;
        String[] strings = (type == OPERATION_TYPE.OPERATION_LIGHT_CHANGE
                || type == OPERATION_TYPE.OPERATION_CUSTOM_BINARIZATION
                || type == OPERATION_TYPE.OPERATION_PERCENT_BLACK_SELECTION_BINARIZATION) ? LIGHT_CHANGE_STRINGS : OTHER_OPERATION_STRINGS;
        Hashtable labelTable = new Hashtable();
        labelTable.put(new Integer(0), new JLabel("0.0"));
        labelTable.put(new Integer(25), new JLabel("2.5"));
        labelTable.put(new Integer(50), new JLabel("5.0"));
        labelTable.put(new Integer(75), new JLabel("7.5"));
        labelTable.put(new Integer(100), new JLabel("10.0"));
        sliders = new JSlider[numOfSliders];
        for (int i = 0; i < sliders.length; i++) {
            sliders[i] = new JSlider(JSlider.HORIZONTAL, minValue, maxValue, initialValue);
            dialogPanel.add(new JLabel(strings[i]));
            dialogPanel.add(sliders[i]);
        }
        Arrays.stream(sliders).forEach((slider) -> {
            slider.setMajorTickSpacing(20);
            slider.setMinorTickSpacing(1);
            slider.setPaintLabels(true);
            slider.setPaintTicks(true);
            if (type == OPERATION_TYPE.OPERATION_MULTIPLICATION || type == OPERATION_TYPE.OPERATION_DIVISION) {
                slider.setLabelTable(labelTable);
            }

        });
        JOptionPane.showConfirmDialog(null, dialogPanel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        returnValues = new int[numOfSliders];
        for (int i = 0; i < numOfSliders; i++) {
            returnValues[i] = sliders[i].getValue();
        }
    }

    public enum OPERATION_TYPE {
        OPERATION_ADDITION,
        OPERATION_SUBSTITUTION,
        OPERATION_MULTIPLICATION,
        OPERATION_DIVISION,
        OPERATION_LIGHT_CHANGE,
        OPERATION_CUSTOM_BINARIZATION,
        OPERATION_PERCENT_BLACK_SELECTION_BINARIZATION
    }
}
