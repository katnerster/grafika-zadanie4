package com.katner.grafika;

import javax.swing.*;
import java.awt.*;

/**
 * Created by michal on 09.04.16.
 */
public class XYInputDialog {
    public static int[] showDialog() {
        JPanel panel = new JPanel();
        SpinnerNumberModel rowModel = new SpinnerNumberModel(0, -1000, 1000, 1);
        JSpinner rowsSpinner = new JSpinner(rowModel);
        SpinnerNumberModel colModel = new SpinnerNumberModel(0, -1000, 1000, 1);
        JSpinner colsSpinner = new JSpinner(colModel);
        panel.setLayout(new GridLayout(2, 2, 5, 5));
        panel.add(new JLabel("x:"));
        panel.add(rowsSpinner);
        panel.add(new JLabel("y:"));
        panel.add(colsSpinner);
        JOptionPane.showConfirmDialog(null, panel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        return new int[]{(Integer) rowsSpinner.getValue(), (Integer) colsSpinner.getValue()};
    }
}
