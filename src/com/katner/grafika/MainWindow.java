package com.katner.grafika;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by michal on 05.03.16.
 */
public class MainWindow {

    final JFileChooser fileChooser = new JFileChooser();
    public JMenuBar menuBar;
    private Image loadedImage = null;
    private JMenu menu;
    private JPanel panel1;
    private JLabel label1;
    private VectorPanel drawingPanel;

    public MainWindow() {
        UIManager.put("OptionPane.minimumSize", new Dimension(500, 200));
        menuBar = new JMenuBar();
        menu = new JMenu("Plik");
        $$$setupUI$$$();
        menuBar.add(menu);
        JMenuItem item = new JMenuItem("Wczytaj plik");
        item.addActionListener(actionEvent -> {
            int returnValue = fileChooser.showOpenDialog(panel1);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                System.out.println(fileChooser.getSelectedFile());
                Image image = null;
                try {
                    image = ImageReader.readImage(fileChooser.getSelectedFile().getPath());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(panel1,
                            "Nie udało się odczytać pliku!",
                            "Błąd",
                            JOptionPane.ERROR_MESSAGE);
                }
                label1.setIcon(new ImageIcon(image.contents));
                panel1.revalidate();
                panel1.repaint();
                loadedImage = image;
            }
        });
        menu.add(item);

        item = new JMenuItem("Wczytaj plik JPG");
        item.addActionListener(actionEvent -> {
            int returnValue = fileChooser.showOpenDialog(panel1);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                System.out.println(fileChooser.getSelectedFile());
                Image image = null;
                try {
                    image = new Image();
                    image.contents = ImageIO.read(fileChooser.getSelectedFile());

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(panel1,
                            "Nie udało się odczytać pliku!",
                            "Błąd",
                            JOptionPane.ERROR_MESSAGE);
                }
                label1.setIcon(new ImageIcon(image.contents));
                panel1.revalidate();
                panel1.repaint();
                loadedImage = image;
            }
        });
        menu.add(item);

        item = new JMenuItem("Zapisz jako JPG");
        item.addActionListener(actionEvent -> {
            int returnValue = fileChooser.showSaveDialog(panel1);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {
                    ImageIO.write(loadedImage.contents, "JPG", fileChooser.getSelectedFile());
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(panel1,
                            "Nie udało się zapisać pliku!",
                            "Błąd",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        menu.add(item);

        item = new JMenuItem("Stwórz pusty obraz");
        item.addActionListener(actionEvent -> {
            XYInputDialog dialog = new XYInputDialog();
            int[] coordinates = XYInputDialog.showDialog();
            loadedImage = new Image();
            loadedImage.width = coordinates[0];
            loadedImage.height = coordinates[1];
            loadedImage.contents = new BufferedImage(coordinates[0], coordinates[1], BufferedImage.TYPE_INT_RGB);
            Graphics2D g = loadedImage.contents.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, loadedImage.contents.getWidth(), loadedImage.contents.getHeight());
            label1.setIcon(new ImageIcon(loadedImage.contents));
            panel1.revalidate();
            panel1.repaint();
        });
        menu.add(item);

        JMenu pointOperationsMenu = new JMenu("Operacje punktowe");
        pointOperationsMenu.setEnabled(true);
        item = new JMenuItem("Dodawanie");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_ADDITION);
                loadedImage.contents = PointOperations.addition(loadedImage.contents, dialog.returnValues[0], dialog.returnValues[1], dialog.returnValues[2]);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);
        item = new JMenuItem("Odejmowanie");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_SUBSTITUTION);
                loadedImage.contents = PointOperations.substitution(loadedImage.contents, dialog.returnValues[0], dialog.returnValues[1], dialog.returnValues[2]);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);
        item = new JMenuItem("Mnożenie");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_MULTIPLICATION);
                loadedImage.contents = PointOperations.multiplication(loadedImage.contents, dialog.returnValues[0] / 10f, dialog.returnValues[1] / 10f, dialog.returnValues[2] / 10f);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);
        item = new JMenuItem("Dzielenie");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_DIVISION);
                loadedImage.contents = PointOperations.division(loadedImage.contents, dialog.returnValues[0] / 10f, dialog.returnValues[1] / 10f, dialog.returnValues[2] / 10f);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);
        item = new JMenuItem("Zmiana jasności");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_LIGHT_CHANGE);
                if (dialog.returnValues[0] < 0) {
                    loadedImage.contents = PointOperations.substitution(loadedImage.contents, -dialog.returnValues[0], -dialog.returnValues[0], -dialog.returnValues[0]);
                } else {
                    loadedImage.contents = PointOperations.addition(loadedImage.contents, dialog.returnValues[0], dialog.returnValues[0], dialog.returnValues[0]);
                }
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);
        item = new JMenuItem("Skala szarości - metoda Luma");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = PointOperations.greyscale(loadedImage.contents, GreyScaleMethod.LUMA_METHOD);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);
        item = new JMenuItem("Skala szarości - metoda average");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = PointOperations.greyscale(loadedImage.contents, GreyScaleMethod.AVERAGE_METHOD);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        item = new JMenuItem("Binaryzacja - Własny wybór progu");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_CUSTOM_BINARIZATION);
                loadedImage.contents = PointOperations.customBinarization(loadedImage.contents, dialog.returnValues[0]);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        item = new JMenuItem("Binaryzacja - Percent Black Selection");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                PointOperationDialog dialog = new PointOperationDialog();
                dialog.showDialog(PointOperationDialog.OPERATION_TYPE.OPERATION_PERCENT_BLACK_SELECTION_BINARIZATION);
                int threshold = PointOperations.percent(loadedImage.contents, dialog.returnValues[0]);
                loadedImage.contents = PointOperations.customBinarization(loadedImage.contents, threshold);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        item = new JMenuItem("Binaryzacja - Mean Iterative Selection");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                int threshold = PointOperations.meanIterative(loadedImage.contents);
                loadedImage.contents = PointOperations.customBinarization(loadedImage.contents, threshold);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        item = new JMenuItem("Binaryzacja - Entropy Selection");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                int threshold = PointOperations.entropySelection(loadedImage.contents);
                loadedImage.contents = PointOperations.customBinarization(loadedImage.contents, threshold);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        item = new JMenuItem("Binaryzacja - Minimum Error");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                int threshold = PointOperations.minimumError(loadedImage.contents);
                loadedImage.contents = PointOperations.customBinarization(loadedImage.contents, threshold);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        item = new JMenuItem("Binaryzacja - Fuzzy Minimum Error");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                int threshold = PointOperations.fuzzyMinimumError(loadedImage.contents);
                loadedImage.contents = PointOperations.customBinarization(loadedImage.contents, threshold);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        pointOperationsMenu.add(item);

        menuBar.add(pointOperationsMenu);

        JMenu filtrationMenu = new JMenu("Filtracja");

        item = new JMenuItem("Filtr wygładzający");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = Convolution.conv(loadedImage.contents, new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}});
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        filtrationMenu.add(item);
        item = new JMenuItem("Filtr medianowy");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = MedianFilter.apply(loadedImage.contents);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        filtrationMenu.add(item);
        item = new JMenuItem("Filtr wykrywania krawędzi(sobel)");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = Convolution.conv(loadedImage.contents, new int[][]{{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}});
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        filtrationMenu.add(item);
        item = new JMenuItem("Filtr górnoprzepustowy wyostrzający");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = Convolution.conv(loadedImage.contents, new int[][]{{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}});
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        filtrationMenu.add(item);
        item = new JMenuItem("Rozmycie gaussowskie");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = Convolution.conv(loadedImage.contents, new int[][]{{1, 2, 1}, {2, 4, 2}, {1, 2, 1}});
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        filtrationMenu.add(item);
        item = new JMenuItem("Splot maski");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                int[] size = MaskSizeDialog.show();
                if (size[0] % 2 == 0 || size[1] % 2 == 0) {
                    JOptionPane.showMessageDialog(panel1,
                            "Maska powinna mieć nieparzyste wymiary!",
                            "Błąd",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                int[][] mask = MaskDialog.showInt(size[0], size[1]);
                loadedImage.contents = Convolution.conv(loadedImage.contents, mask);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });
        filtrationMenu.add(item);
        menuBar.add(filtrationMenu);

        JMenu histogramMenu = new JMenu("Histogram");
        item = new JMenuItem("Equalize");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = Histogram.histogramOperation(loadedImage.contents, Histogram.OPERATION_TYPE.HISTOGRAM_EQUALIZATION);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });
        histogramMenu.add(item);
        item = new JMenuItem("Strech");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                loadedImage.contents = Histogram.histogramOperation(loadedImage.contents, Histogram.OPERATION_TYPE.HISTOGRAM_STRECH);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });
        histogramMenu.add(item);

        JMenu morphologyMenu = new JMenu("Morphology");
        item = new JMenuItem("Dilation");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                Item[][] mask = new Item[][]{
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                };
                loadedImage.contents = BufferedImageArrayConverter.toImage(MorphologicalOperator.dilate(BufferedImageArrayConverter.toArray(loadedImage.contents), mask));
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });

        morphologyMenu.add(item);

        item = new JMenuItem("Erosion");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                Item[][] mask = new Item[][]{
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                };
                loadedImage.contents = BufferedImageArrayConverter.toImage(MorphologicalOperator.erode(BufferedImageArrayConverter.toArray(loadedImage.contents), mask));
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });
        morphologyMenu.add(item);

        item = new JMenuItem("Opening");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                Item[][] mask = new Item[][]{
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                };
                loadedImage.contents = BufferedImageArrayConverter.toImage(MorphologicalOperator.open(BufferedImageArrayConverter.toArray(loadedImage.contents), mask));
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });
        morphologyMenu.add(item);

        item = new JMenuItem("Closing");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                Item[][] mask = new Item[][]{
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                };
                loadedImage.contents = BufferedImageArrayConverter.toImage(MorphologicalOperator.close(BufferedImageArrayConverter.toArray(loadedImage.contents), mask));
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();

            }
        });
        morphologyMenu.add(item);

        item = new JMenuItem("Thinning");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                Item[][][] masks = new Item[][][]{
                        {
                                {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                        },

                        {
                                {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                        },

                        {
                                {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                                {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND}
                        },

                        {
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                                {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND}
                        },

                        {
                                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND}
                        },

                        {
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE}
                        },


                        {
                                {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND},
                                {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND}
                        },

                        {
                                {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE},
                                {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                        }
                };
                loadedImage.contents = BufferedImageArrayConverter.toImage(MorphologicalOperator.thin(BufferedImageArrayConverter.toArray(loadedImage.contents), masks));
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        morphologyMenu.add(item);

        item = new JMenuItem("Thickenning");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                Item[][][] masks = new Item[][][]{
                        {
                                {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                        },

                        {
                                {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                        },

                        {
                                {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                                {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND}
                        },

                        {
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                                {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND}
                        },

                        {
                                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND}
                        },

                        {
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                                {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE}
                        },


                        {
                                {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND},
                                {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND}
                        },

                        {
                                {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE},
                                {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                        }
                };
                loadedImage.contents = BufferedImageArrayConverter.toImage(MorphologicalOperator.thick(BufferedImageArrayConverter.toArray(loadedImage.contents), masks));
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
            }
        });
        morphologyMenu.add(item);

        menuBar.add(histogramMenu);
        menuBar.add(morphologyMenu);

        JMenu colorizeMenu = new JMenu("Koloryzacja");
        item = new JMenuItem("Procent zielonych terenów");
        item.addActionListener(actionEvent -> {
            if (loadedImage != null) {
                GreenColorizer colorizer = new GreenColorizer();
                loadedImage.contents = colorizer.colorize(loadedImage.contents);
                label1.setIcon(new ImageIcon(loadedImage.contents));
                panel1.revalidate();
                panel1.repaint();
                JOptionPane.showMessageDialog(panel1,
                        String.format("Na obrazie znaleziono %f%% zielonych terenów.", colorizer.percentOfGreen),
                        "Procent zielonych terenów",
                        JOptionPane.INFORMATION_MESSAGE);

            }
        });
        colorizeMenu.add(item);

        menuBar.add(colorizeMenu);

        JMenu twoDimensionMenu = new JMenu("Przekształcenia 2D");
        menuBar.add(twoDimensionMenu);

        item = new JMenuItem("Przesunięcie o wektor");
        item.addActionListener(actionEvent -> {
            int[] coordinates = XYInputDialog.showDialog();
            drawingPanel.translate(coordinates[0], coordinates[1]);
        });
        twoDimensionMenu.add(item);

        item = new JMenuItem("Obrót");
        item.addActionListener(actionEvent ->
        {
            int[] coordinates = XYInputDialog.showDialog();
            drawingPanel.rotate(coordinates[0], coordinates[1], AngleInputDialog.showDialog());
        });
        twoDimensionMenu.add(item);

        item = new JMenuItem("Skalowanie");
        item.addActionListener(actionEvent -> {
            int[] coordinates = XYInputDialog.showDialog();
            drawingPanel.scale(coordinates[0], coordinates[1], ScaleInputDialog.showDialog());
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Zaakceptuj");
        item.addActionListener(actionEvent -> {
            loadedImage.contents = drawingPanel.drawToImage(loadedImage.contents);
            label1.setIcon(new ImageIcon(loadedImage.contents));
            panel1.revalidate();
            panel1.repaint();
            drawingPanel.clearPoints();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Rysowanie obiektu wektorowego");
        item.addActionListener(actionEvent -> {
            drawingPanel.mode = VectorPanel.MODE.VECTOR;
            drawingPanel.clearPoints();
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Rysowanie krzywej Beziera");
        item.addActionListener(actionEvent -> {
            drawingPanel.mode = VectorPanel.MODE.BEZIER;
            drawingPanel.clearPoints();
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Brak rysowania");
        item.addActionListener(actionEvent -> {
            drawingPanel.mode = null;
            drawingPanel.clearPoints();
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Prostokąty");
        item.addActionListener(actionEvent -> {
            drawingPanel.mode = VectorPanel.MODE.OBJECT;
            drawingPanel.objectType = type.RECTANGLE;
            drawingPanel.clearPoints();
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);


        item = new JMenuItem("Prostokąty - podanie współrzędnych punktów");
        item.addActionListener(actionEvent -> {
            int[] coordinatesFirst = XYInputDialog.showDialog();
            int[] coordinatesSecond = XYInputDialog.showDialog();
            DrawingAction action = new DrawingAction();
            action.typ = type.RECTANGLE;
            action.p1 = new Point(coordinatesFirst[0], coordinatesFirst[1]);
            action.p2 = new Point(coordinatesSecond[0], coordinatesSecond[1]);
            drawingPanel.addObject(action);
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Linie");
        item.addActionListener(actionEvent -> {
            drawingPanel.mode = VectorPanel.MODE.OBJECT;
            drawingPanel.objectType = type.LINE;
            drawingPanel.clearPoints();
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Linie - podanie współrzędnych punktów");
        item.addActionListener(actionEvent -> {
            int[] coordinatesFirst = XYInputDialog.showDialog();
            int[] coordinatesSecond = XYInputDialog.showDialog();
            DrawingAction action = new DrawingAction();
            action.typ = type.LINE;
            action.p1 = new Point(coordinatesFirst[0], coordinatesFirst[1]);
            action.p2 = new Point(coordinatesSecond[0], coordinatesSecond[1]);
            drawingPanel.addObject(action);
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Elipsy");
        item.addActionListener(actionEvent -> {
            drawingPanel.mode = VectorPanel.MODE.OBJECT;
            drawingPanel.objectType = type.CIRCLE;
            drawingPanel.clearPoints();
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);

        item = new JMenuItem("Elipsy - podanie współrzędnych punktów");
        item.addActionListener(actionEvent -> {
            int[] coordinatesFirst = XYInputDialog.showDialog();
            int[] coordinatesSecond = XYInputDialog.showDialog();
            DrawingAction action = new DrawingAction();
            action.typ = type.CIRCLE;
            action.p1 = new Point(coordinatesFirst[0], coordinatesFirst[1]);
            action.p2 = new Point(coordinatesSecond[0], coordinatesSecond[1]);
            drawingPanel.addObject(action);
            drawingPanel.repaint();
        });

        twoDimensionMenu.add(item);


    }

    public static void main(String[] args) {
        MainWindow window = new MainWindow();
        JFrame frame = new JFrame("MainWindow");
        frame.setJMenuBar(window.menuBar);
        frame.setContentPane(window.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        label1 = new JLabel();
        // TODO: place custom component creation code here
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
     * edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        panel1 = new JPanel();
        panel1.setLayout(new BorderLayout(0, 0));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel1.add(scrollPane1, BorderLayout.CENTER);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        scrollPane1.setViewportView(panel2);
        label1.setDoubleBuffered(true);
        label1.setName("imageLabel");
        label1.setText("");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel2.add(label1, gbc);
        final JPanel spacer1 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel2.add(spacer1, gbc);
        final JPanel spacer2 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.VERTICAL;
        panel2.add(spacer2, gbc);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }

}
