/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katner.grafika;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.IntStream;

/**
 * @author katnerm
 */
public class VectorPanel extends JPanel {
    public MODE mode = MODE.OBJECT;
    public type objectType = type.LINE;
    private ArrayList<Rectangle> rectangles = new ArrayList<>();
    private Rectangle movedRectangle = null;
    private ArrayList<DrawingAction> drawingActionList = new ArrayList<>();
    private Point p1, p2;

    public VectorPanel() {
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (mode != null && mode != MODE.OBJECT) {
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        Iterator<Rectangle> it = rectangles.iterator();
                        while (it.hasNext()) {
                            Rectangle rect = it.next();
                            if (rect.contains(e.getX(), e.getY())) {
                                it.remove();
                            }
                        }
                    } else {
                        rectangles.add(new Rectangle(e.getX() - 5, e.getY() - 5, 10, 10));
                    }
                    repaint();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (mode == MODE.OBJECT) {
                    p1 = e.getPoint();
                    repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                movedRectangle = null;
                if (mode == MODE.OBJECT) {
                    p2 = e.getPoint();
                    DrawingAction action = new DrawingAction();
                    action.p1 = p1;
                    action.p2 = p2;
                    action.typ = objectType;
                    drawingActionList.add(action);
                    repaint();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        this.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (mode != null) {
                    if (movedRectangle == null) {
                        for (Rectangle rect : rectangles) {
                            if (rect.contains(e.getX(), e.getY())) {
                                movedRectangle = rect;
                            }
                        }
                    }
                    if (movedRectangle != null) {
                        movedRectangle.setLocation(e.getX() - 5, e.getY() - 5);
                        repaint();
                    }
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
    }

    public static long newtonBinomial(int n, int k) {
        if (k > n - k) {
            k = n - k;
        }

        long b = 1;
        for (int i = 1, m = n; i <= k; i++, m--) {
            b = b * m / i;
        }
        return b;
    }

    @Override
    protected void paintChildren(Graphics g) {
        super.paintChildren(g);
        Graphics2D g2 = (Graphics2D) g;
        paintContent(g2, true);
    }

    public void translate(int x, int y) {
        for (Rectangle rect : rectangles) {
            rect.setLocation(rect.x + x, rect.y + y);
        }
        repaint();
    }

    public void rotate(int x0, int y0, double alpha) {
        for (Rectangle rect : rectangles) {
            int x = rect.x;
            int y = rect.y;
            double newX = Math.cos(Math.toRadians(alpha)) * (x - x0) - Math.sin(Math.toRadians(alpha)) * (y - y0) + x0;
            double newY = Math.sin(Math.toRadians(alpha)) * (x - x0) + Math.cos(Math.toRadians(alpha)) * (y - y0) + y0;
            rect.setLocation((int) newX, (int) newY);
        }
        repaint();
    }

    public void scale(int x0, int y0, double k) {
        for (Rectangle rect : rectangles) {
            int x = rect.x;
            int y = rect.y;
            int newX = (int) (x * k + (1 - k) * x0);
            int newY = (int) (y * k + (1 - k) * y0);
            System.out.println(newX);
            System.out.println(newY);
            rect.setLocation(newX, newY);
        }
        repaint();
    }

    public void clearPoints() {
        rectangles.clear();
        drawingActionList.clear();
    }

    public BufferedImage drawToImage(BufferedImage contents) {
        Graphics2D g = contents.createGraphics();
        paintContent(g, false);
        return contents;
    }

    private void paintContent(Graphics2D g2, boolean drawControlPoints) {
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        g2.setColor(Color.BLACK);
//        if(mode == MODE.OBJECT)
//        {
        for (DrawingAction act : drawingActionList) {
            switch (act.typ) {
                case LINE:
                    g2.drawLine(act.p1.x, act.p1.y, act.p2.x, act.p2.y);
                    break;
                case RECTANGLE:
                    g2.drawRect(act.p1.x, act.p1.y, act.p2.x - act.p1.x, act.p2.y - act.p1.y);
                    break;
                case CIRCLE:
                    g2.drawOval(act.p1.x, act.p1.y, act.p2.x - act.p1.x, act.p2.y - act.p1.y);
                    break;
            }
        }
//        }
        if (drawControlPoints) {
            for (Rectangle rect : rectangles) {
                g2.fillRect(rect.x, rect.y, rect.width, rect.height);
            }
        }

        if (mode == MODE.BEZIER) {
            bezier(g2);
        }

        g2.setColor(Color.lightGray);
        if (mode == MODE.VECTOR || (mode == MODE.BEZIER && drawControlPoints)) {
            for (int i = 1; i < rectangles.size(); i++) {
                Rectangle prev = rectangles.get(i - 1);
                Rectangle next = rectangles.get(i);
                g2.drawLine(prev.x + 5, prev.y + 5, next.x + 5, next.y + 5);
            }
        }
        if (mode == MODE.VECTOR) {
            if (rectangles.size() > 1) {
                g2.drawLine(rectangles.get(0).x + 5,
                        rectangles.get(0).y + 5,
                        rectangles.get(rectangles.size() - 1).x + 5,
                        rectangles.get(rectangles.size() - 1).y + 5);
            }
        }


    }

    private void bezier(Graphics2D g2) {
        if (rectangles.size() >= 2) {
            RenderingHints rh = new RenderingHints(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHints(rh);
            ArrayList<Point> points = new ArrayList<>();
            for (double t = 0.0; t < 1.0; t += 0.001) {
                double tt = t;
                int n = rectangles.size() - 1;
                double x = IntStream.rangeClosed(0, n).mapToDouble(i -> newtonBinomial(n, i) * Math.pow(1 - tt, n - i) * Math.pow(tt, i) * (rectangles.get(i).x + 5)).sum();
                double y = IntStream.rangeClosed(0, n).mapToDouble(i -> newtonBinomial(n, i) * Math.pow(1 - tt, n - i) * Math.pow(tt, i) * (rectangles.get(i).y + 5)).sum();
                points.add(new Point((int) x, (int) y));
            }
            g2.setColor(Color.red);
            for (int i = 1; i < points.size(); i++) {
                g2.drawLine(points.get(i - 1).x, points.get(i - 1).y, points.get(i).x, points.get(i).y);
            }
        }
    }

    public void addObject(DrawingAction action) {
        drawingActionList.add(action);
    }

    public enum MODE {VECTOR, BEZIER, OBJECT}
}
