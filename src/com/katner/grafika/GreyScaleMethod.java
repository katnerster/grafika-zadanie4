package com.katner.grafika;

public enum GreyScaleMethod {
    LUMA_METHOD,
    AVERAGE_METHOD
}
