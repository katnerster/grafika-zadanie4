package com.katner.grafika;

import javax.swing.*;
import java.awt.*;

/**
 * Created by michal on 09.04.16.
 */
public class ScaleInputDialog {
    public static double showDialog() {
        JPanel panel = new JPanel();
        SpinnerNumberModel rowModel = new SpinnerNumberModel(1, 0, 15, 0.01);
        JSpinner rowsSpinner = new JSpinner(rowModel);
        panel.setLayout(new GridLayout(2, 2, 5, 5));
        panel.add(new JLabel("Skala:"));
        panel.add(rowsSpinner);
        JOptionPane.showConfirmDialog(null, panel, "ScreenPreview", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        return (Double) rowsSpinner.getValue();
    }
}
