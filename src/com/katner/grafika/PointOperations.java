package com.katner.grafika;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.stream.IntStream;

import static java.lang.StrictMath.log;

/**
 * Created by michal on 13.03.16.
 */
public class PointOperations {

    static BufferedImage greyscale(BufferedImage source, GreyScaleMethod method) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        int gray = 0;
        for (int x = 0; x < source.getWidth(); x++) {
            for (int y = 0; y < source.getHeight(); y++) {
                Color mycolor = new Color(source.getRGB(x, y));
                switch (method) {
                    case LUMA_METHOD:
                        gray = (int) (mycolor.getRed() * 0.3 + mycolor.getGreen() * 0.59
                                + mycolor.getBlue() * 0.11);
                        break;
                    case AVERAGE_METHOD:
                        gray = (int) ((mycolor.getRed() + mycolor.getGreen() + mycolor.getBlue()) / 3f);
                        break;
                }
                gray = (gray << 16) + (gray << 8) + gray;
                result.setRGB(x, y, gray);
            }
        }
        return result;
    }

    static BufferedImage addition(BufferedImage source, int red, int green, int blue) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < source.getWidth(); x++) {
            for (int y = 0; y < source.getHeight(); y++) {
                Color mycolor = new Color(source.getRGB(x, y));
                int outputRed = mycolor.getRed() + red;
                int outputGreen = mycolor.getGreen() + green;
                int outputBlue = mycolor.getBlue() + blue;
                if (outputRed > 255) {
                    outputRed = 255;
                }
                if (outputGreen > 255) {
                    outputGreen = 255;
                }
                if (outputBlue > 255) {
                    outputBlue = 255;
                }
                result.setRGB(x, y, (outputRed << 16) + (outputGreen << 8) + outputBlue);
            }
        }
        return result;
    }

    static BufferedImage substitution(BufferedImage source, int red, int green, int blue) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < source.getWidth(); x++) {
            for (int y = 0; y < source.getHeight(); y++) {
                Color mycolor = new Color(source.getRGB(x, y));
                int outputRed = mycolor.getRed() - red;
                int outputGreen = mycolor.getGreen() - green;
                int outputBlue = mycolor.getBlue() - blue;
                if (outputRed < 0) {
                    outputRed = 0;
                }
                if (outputGreen < 0) {
                    outputGreen = 0;
                }
                if (outputBlue < 0) {
                    outputBlue = 0;
                }
                result.setRGB(x, y, (outputRed << 16) + (outputGreen << 8) + outputBlue);
            }
        }

        return result;
    }

    static BufferedImage multiplication(BufferedImage source, double red, double green, double blue) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < source.getWidth(); x++) {
            for (int y = 0; y < source.getHeight(); y++) {
                Color mycolor = new Color(source.getRGB(x, y));
                double outputRed = mycolor.getRed() * red;
                double outputGreen = mycolor.getGreen() * green;
                double outputBlue = mycolor.getBlue() * blue;

                if (outputRed < 0) {
                    outputRed = 0;
                }
                if (outputRed > 255) {
                    outputRed = 255;
                }

                if (outputGreen < 0) {
                    outputGreen = 0;
                }
                if (outputGreen > 255) {
                    outputGreen = 255;
                }

                if (outputBlue < 0) {
                    outputBlue = 0;
                }
                if (outputBlue > 255) {
                    outputBlue = 255;
                }
                result.setRGB(x, y, ((int) outputRed << 16) + ((int) outputGreen << 8) + (int) outputBlue);
            }
        }
        return result;
    }

    static BufferedImage division(BufferedImage source, double red, double green, double blue) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        int gray = 0;
        for (int x = 0; x < source.getWidth(); x++) {
            for (int y = 0; y < source.getHeight(); y++) {
                Color mycolor = new Color(source.getRGB(x, y));
                double outputRed = mycolor.getRed() / red;
                double outputGreen = mycolor.getGreen() / green;
                double outputBlue = mycolor.getBlue() / blue;

                if (outputRed < 0) {
                    outputRed = 0;
                }
                if (outputRed > 255) {
                    outputRed = 255;
                }

                if (outputGreen < 0) {
                    outputGreen = 0;
                }
                if (outputGreen > 255) {
                    outputGreen = 255;
                }

                if (outputBlue < 0) {
                    outputBlue = 0;
                }
                if (outputBlue > 255) {
                    outputBlue = 255;
                }
                result.setRGB(x, y, ((int) outputRed << 16) + ((int) outputGreen << 8) + (int) outputBlue);
            }
        }
        return result;
    }

    public static BufferedImage customBinarization(BufferedImage source, int threshold) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        int gray = 0;
        for (int x = 0; x < source.getWidth(); x++) {
            for (int y = 0; y < source.getHeight(); y++) {
                Color mycolor = new Color(source.getRGB(x, y));
                if (mycolor.getRed() < threshold) {
                    gray = 0;
                } else {
                    gray = 255;
                }
                result.setRGB(x, y, (gray << 16) + (gray << 8) + gray);
            }
        }
        return result;
    }

    public static int percent(BufferedImage source, int percent) {
        int[] hist = Histogram.imageHistogram(source).get(0);
        int i = source.getHeight() * source.getWidth() * percent / 100;
        int j = 0;
        int k = 0;
        do {
            j += hist[k];
            if (j > i) {
                break;
            }
            k++;
        } while (k < 256);
        return Math.min(255, k);
    }

    public static int meanIterative(BufferedImage source) {
        int[] hist = Histogram.imageHistogram(source).get(0);
        int tNext = IntStream.range(0, hist.length)
                .map(i -> hist[i] * i).sum();
        tNext /= source.getHeight() * source.getWidth();
        int tPrev;
        int objectLiczebnik;
        int backgroundLiczebnik;
        do {
            tPrev = tNext;
            int backgroundMianownik;
            int objectMianownik;
            objectLiczebnik = objectMianownik = backgroundLiczebnik = backgroundMianownik = 0;
            for (int i = 0; i <= tPrev; i++) {
                objectLiczebnik += i * hist[i];
                objectMianownik += hist[i];
            }
            objectMianownik += objectMianownik;

            for (int i = tPrev + 1; i < 256; i++) {
                backgroundLiczebnik += i * hist[i];
                backgroundMianownik += hist[i];
            }

            backgroundMianownik += backgroundMianownik;
            tNext = objectLiczebnik / objectMianownik + backgroundLiczebnik / backgroundMianownik;
        } while (tNext != tPrev);
        return tNext;
    }

    public static int entropySelection(BufferedImage source) {
//        http://www.mif.pg.gda.pl/homepages/marcin/Wyklad3.pdf strona 16
        int thresholdIdx = Integer.MIN_VALUE;
        double threshold = Double.MIN_VALUE;
        int[] histogram = Histogram.imageHistogram(source).get(0);
        int sum = Arrays.stream(histogram).sum();
        double[] normalizedHist = IntStream.range(0, histogram.length).mapToDouble(i -> histogram[i] / (double) sum).toArray();
        double[] pT = new double[histogram.length];
        pT[0] = normalizedHist[0];
        for (int i = 1; i < histogram.length; i++) {
            pT[i] = pT[i - 1] + normalizedHist[i];
        }
        double[] pob = IntStream.range(0, histogram.length).mapToDouble(i -> IntStream.range(0, i).mapToDouble(i1 -> normalizedHist[i1]).sum()).toArray();
        double[] pb = IntStream.range(0, histogram.length).mapToDouble(i -> 1 - pob[i]).toArray();
        double[] h = IntStream.range(0, histogram.length).mapToDouble(i -> -pob[i] * logn(pob[i], 2) - pb[i] * logn(pb[i], 2)).toArray();

        for (int i = 0; i < h.length; i++) {
            if (h[i] > threshold) {
                threshold = h[i];
                thresholdIdx = i;
            }
        }
        return thresholdIdx;
    }

    public static double logn(double n, int base) {
        return (Math.log(n) / Math.log(base));
    }

    public static int minimumError(BufferedImage source) {
        int[] data = new int[source.getHeight() * source.getWidth()];
        int tmp = 0;
        for (int i = 0; i < source.getHeight(); i++) {
            for (int j = 0; j < source.getWidth(); j++) {
                Color color = new Color(source.getRGB(i, j));
                data[tmp++] = color.getRed();
            }
        }
        int[] histogram = Histogram.imageHistogram(source).get(0);
        int sum = Arrays.stream(histogram).sum();
        double[] j = new double[256];
        double[] normalizedHist = IntStream.range(0, histogram.length).mapToDouble(i -> histogram[i] / (double) sum).toArray();
        for (int i = 0; i < 255; i++) {
            int[] hist1 = Arrays.copyOfRange(histogram, 0, i);
            int[] hist2 = Arrays.copyOfRange(histogram, i + 1, 256);
            int P1 = Arrays.stream(hist1).sum();
            int P2 = Arrays.stream(hist2).sum();
            if (P1 > 0 && P2 > 0) {
                int sum1 = Arrays.stream(hist1).sum();
                int sum2 = Arrays.stream(hist2).sum();
                double mean1 = IntStream.range(0, hist1.length).mapToDouble(b -> hist1[b] * b).sum() / sum1;
                double mean2 = IntStream.range(0, hist2.length).mapToDouble(b -> hist2[b] * b).sum() / sum2;

                double sigma1 = Math.sqrt(IntStream.range(0, hist1.length).mapToDouble(c -> hist1[c] * Math.pow(c - mean1, 2)).sum() / sum1);
                double sigma2 = Math.sqrt(IntStream.range(0, hist2.length).mapToDouble(c -> hist2[c] * Math.pow(c - mean2, 2)).sum() / sum2);
                if (sigma1 > 0 && sigma2 > 0) {

                    j[i] = 1 + 2 * (P1 * log(sigma1) + P2 * log(sigma2)) - 2 * (P1 * log(P1) + P2 * log(P2));
                }
            }
        }
        int thresholdIdx = Integer.MIN_VALUE;
        double threshold = Double.MAX_VALUE;
        for (int i = 0; i < j.length; i++) {
            if (j[i] < threshold) {
                threshold = j[i];
                thresholdIdx = i;
            }
        }
        return thresholdIdx;
    }

    /**
     * Huang and Wang
     * https://imagej.nih.gov/ij/developer/source/ij/process/AutoThresholder.java.html
     *
     * @param source
     * @return
     */
    public static int fuzzyMinimumError(BufferedImage source) {
        int[] histogram = Histogram.imageHistogram(source).get(0);
        int numOfPixels = Arrays.stream(histogram).sum();
        int minimum = 0;
        int maximum = 256;
        for (int ih = 0; ih < 256; ih++) {
            if (histogram[ih] != 0) {
                minimum = ih;
                break;
            }
        }

        for (int ih = 255; ih >= minimum; ih--) {
            if (histogram[ih] != 0) {
                maximum = ih;
                break;
            }
        }
        double C = 1.0 / (maximum - minimum);
        double[] mu_0 = new double[256];
        int num_pix = 0;
        int sum_pix = 0;
        for (int i = minimum; i < 256; i++) {
            sum_pix += (double) i * histogram[i];
            num_pix += histogram[i];
            /* NUM_PIX cannot be zero ! */
            mu_0[i] = sum_pix / num_pix;
        }

        double[] mu_1 = new double[256];
        sum_pix = num_pix = 0;
        for (int i = maximum; i > 0; i--) {
            sum_pix += (double) i * histogram[i];
            num_pix += histogram[i];
            /* NUM_PIX cannot be zero ! */
            mu_1[i - 1] = sum_pix / (double) num_pix;
        }

        double[] entropies = new double[256];
        for (int i = 0; i < 256; i++) {
            double entropy = 0.0;

            for (int ih = 0; ih <= i; ih++) {
                double mu_x = ux(ih, i, C, mu_0);
                if (!((mu_x < 1e-06) || (mu_x > 0.999999))) {
                    entropy += histogram[ih] * shannon(mu_x);
                }
            }

            for (int ih = i + 1; ih < 256; ih++) {
                double mu_x = 1.0 / (1.0 + C * Math.abs(ih - mu_1[i]));
                if (!((mu_x < 1e-06) || (mu_x > 0.999999))) {
                    entropy += histogram[ih] * shannon(mu_x);
                }
            }
            entropies[i] = entropy;
        }
        int thresholdIdx = Integer.MIN_VALUE;
        double threshold = Double.MAX_VALUE;
        for (int i = 0; i < entropies.length; i++) {
            if (entropies[i] < threshold) {
                threshold = entropies[i];
                thresholdIdx = i;
            }
        }
        return thresholdIdx;
    }

    private static double shannon(double x) {
        return -x * Math.log(x) - (1.0 - x) * Math.log(1.0 - x);
    }

    private static double ux(int i, int t, double C, double[] u) {
        double mu_x;
        mu_x = 1.0 / (1.0 + C * Math.abs(i - u[t]));
        return mu_x;
    }
}
