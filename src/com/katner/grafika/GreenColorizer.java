package com.katner.grafika;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.katner.grafika.Util.copyImage;

/**
 * Created by michal on 16.05.16.
 */
public class GreenColorizer {
    private static int idealG = 103;
    private static int idealR = 75;
    private static int idealB = 89;
    public float percentOfGreen = 0;

    public BufferedImage colorize(BufferedImage source) {
        BufferedImage output = copyImage(source);
        long notIgnoredPixels = 0;
        long green = 0;
        for (int i = 0; i < source.getWidth(); i++) {
            for (int j = 0; j < source.getHeight(); j++) {
                if (source.getRGB(i, j) != Color.BLACK.getRGB()) {
                    notIgnoredPixels++;
                }
            }
        }
        for (int i = 0; i < source.getWidth(); i++) {
            for (int j = 0; j < source.getHeight(); j++) {
                Color c = new Color(source.getRGB(i, j));
                int g = Math.abs(c.getGreen() - idealG);
                int r = Math.abs(c.getRed() - idealR);
                int b = Math.abs(c.getBlue() - idealB);
                if (c != Color.BLACK && r < 60 && g < 60 && b < 60) {
                    green++;
                    output.setRGB(i, j, Color.GREEN.getRGB());
                }
            }
        }
        percentOfGreen = (float) green / notIgnoredPixels * 100;
        return output;
    }
}
