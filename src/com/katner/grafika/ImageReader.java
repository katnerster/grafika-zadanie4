package com.katner.grafika;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Scanner;

/**
 * Created by michal on 25.02.16.
 */
public class ImageReader {
    public static Image readImage(String path) throws Exception {
        Image img = new Image();
        FileInputStream is = new FileInputStream(path);
        BufferedReader fr = new BufferedReader(new InputStreamReader(is));
        Scanner scanner = new Scanner(is);
        while (!img.isInfoComplete()) {
            String tmp = getLineWithoutComment(scanner);
            Scanner sc = new Scanner(tmp);
            while (sc.hasNext()) {
                if (!img.hasTypeInfo()) {
                    img.type = sc.next();
                } else {
                    if (sc.hasNextInt())
                        img.addInfo(sc.nextInt());
                    else if (sc.next().startsWith("#")) {
                        continue;
                    }
                }
            }
        }

        long startTime = System.nanoTime();
        if (img.type.equals("P3")) {
            readP3File(img, scanner);
        } else if (img.type.equals("P6")) {
            readP6File(path, img, is, scanner);
        } else {
            System.out.println("Nieznany format pliku!");
            throw new Exception();
        }
        long endTime = System.nanoTime();

        long duration = (endTime - startTime);
        img.loadingTime = (float) duration / 1000000000;
        System.out.println(img.loadingTime);
        return img;
    }

    private static void readP3File(Image img, Scanner scanner) {
        Pixel[][] pixels = new Pixel[img.height][img.width];
        int x = 0;
        while (scanner.hasNext() && x < img.height) {

            for (int i = 0; i < img.width; i++) {
                if (pixels[x][i] == null) {
                    pixels[x][i] = new Pixel();
                }
                while (scanner.hasNext("#")) {
                    String l = scanner.nextLine();
                    System.out.println(l);
                }
                if (scanner.hasNextInt())
                    pixels[x][i].r = scanner.nextInt();
                while (scanner.hasNext("#")) {
                    String l = scanner.nextLine();
                    System.out.println(l);
                }
                if (scanner.hasNextInt())
                    pixels[x][i].g = scanner.nextInt();
                while (scanner.hasNext("#")) {
                    String l = scanner.nextLine();
                    System.out.println(l);
                }
                if (scanner.hasNextInt())
                    pixels[x][i].b = scanner.nextInt();
            }
            x++;
        }
        BufferedImage image = new BufferedImage(img.width, img.height, BufferedImage.TYPE_INT_RGB);
        for (x = 0; x < img.height; x++) {
            for (int y = 0; y < img.width; y++) {
                int r = pixels[x][y].r;
                int g = pixels[x][y].g;
                int b = pixels[x][y].b;

                r = r & 0xffff;
                g = g & 0xffff;
                b = b & 0xffff;
                int red = Integer.divideUnsigned(r * 255, img.maxValue);
                int green = Integer.divideUnsigned(g * 255, img.maxValue);
                int blue = Integer.divideUnsigned(b * 255, img.maxValue);

                int color = ((red & 0xff) << 16) | ((green & 0xff) << 8) | (blue & 0xff);
                image.setRGB(y, x, color);
            }
        }
        img.contents = image;
    }

    private static void readP6File(String path, Image img, FileInputStream is, Scanner scanner) throws IOException {
        int dataOffset = scanner.match().end();
        scanner.close();
        is.close();
        is = new FileInputStream(path);
        is.skip(dataOffset);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(is);
        BufferedImage image = new BufferedImage(img.width, img.height, BufferedImage.TYPE_INT_RGB);
        if (img.maxValue < 256) {
            byte[] pixelsbytes = new byte[img.width * 3];
            for (int i = 0; i < img.height; i++) {

                bufferedInputStream.read(pixelsbytes);
                for (int j = 0; j < (pixelsbytes.length / 3); j++) {
                    byte red = (byte) ((pixelsbytes[j * 3] * 255) / img.maxValue);
                    byte green = (byte) ((pixelsbytes[j * 3 + 1] * 255) / img.maxValue);
                    byte blue = (byte) ((pixelsbytes[j * 3 + 2] * 255) / img.maxValue);
                    int color = ((red & 0xff) << 16) | ((green & 0xff) << 8) | (blue & 0xff);
                    image.setRGB(j, i, color);
                }
            }
        } else {
            byte[] pixelsbytes = new byte[img.width * 3 * 2];
            for (int i = 0; i < img.height; i++) {

                bufferedInputStream.read(pixelsbytes);
                for (int j = 0; j < (pixelsbytes.length / 6); j++) {

                    int r = (pixelsbytes[j * 6] << 8) + pixelsbytes[j * 6 + 1];
                    int g = (pixelsbytes[j * 6 + 2] << 8) + pixelsbytes[j * 6 + 3];
                    int b = (pixelsbytes[j * 6 + 4] << 8) + pixelsbytes[j * 6 + 5];

                    r = r & 0xffff;
                    g = g & 0xffff;
                    b = b & 0xffff;
                    int red = Integer.divideUnsigned(r * 255, img.maxValue);
                    int green = Integer.divideUnsigned(g * 255, img.maxValue);
                    int blue = Integer.divideUnsigned(b * 255, img.maxValue);

                    int color = ((red & 0xff) << 16) | ((green & 0xff) << 8) | (blue & 0xff);
                    image.setRGB(j, i, color);
                }
            }
        }

        img.contents = image;
    }

    private static String getLineWithoutComment(Scanner reader) throws IOException {
        String tmp = null;
        do {
            tmp = reader.nextLine();
        } while (tmp.startsWith("#"));
        return tmp;
    }
}
