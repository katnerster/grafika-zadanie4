package com.katner.grafika;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

/**
 * Created by michal on 23.03.16.
 */
public class MedianFilter {

    private static int applyMask(BufferedImage source, int x, int y) {
        BufferedImage subImage = source.getSubimage(x - 1, y - 1, 3, 3);
        int[] reds = new int[9];
        int[] greens = new int[9];
        int[] blues = new int[9];
        int i = 0;
        for (int forx = 0; forx < 3; forx++) {
            for (int fory = 0; fory < 3; fory++) {
                Color color = new Color(subImage.getRGB(forx, fory));
                reds[i] = color.getRed();
                greens[i] = color.getGreen();
                blues[i++] = color.getBlue();
            }
        }
        Arrays.sort(reds);
        Arrays.sort(greens);
        Arrays.sort(blues);

        return (median(reds) << 16) + (median(greens) << 8) + median(blues);
    }

    private static int median(int[] m) {
        int middle = m.length / 2;
        if (m.length % 2 == 1) {
            return m[middle];
        } else {
            return (m[middle - 1] + m[middle]) / 2;
        }
    }

    public static BufferedImage apply(BufferedImage source) {
        BufferedImage result = new BufferedImage(
                source.getWidth(),
                source.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics tmpGraphics = result.getGraphics();
        tmpGraphics.drawImage(source, 0, 0, null);
        tmpGraphics.dispose();
        for (int x = 1; x < source.getWidth() - 1; x++) {
            for (int y = 1; y < source.getHeight() - 1; y++) {
                result.setRGB(x, y, applyMask(source, x, y));
            }
        }
        return result;
    }
}
