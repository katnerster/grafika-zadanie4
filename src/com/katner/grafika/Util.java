package com.katner.grafika;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author katnerm
 */
public class Util {
    public static boolean[][] createArray(int x, int y) {
        boolean[][] result = new boolean[y][];
        for (int i = 0; i < y; i++) {
            result[i] = new boolean[x];
        }
        return result;
    }

    public static boolean[][] createArray(boolean[][] input) {
        return createArray(input[0].length, input.length);
    }

    public static boolean[][] complement(boolean[][] input) {
        boolean[][] result = Util.createArray(input);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = !input[i][j];
            }
        }
        return result;
    }

    public static boolean[][] sum(boolean[][] inputA, boolean[][] inputB) {
        boolean[][] result = Util.createArray(inputA);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = inputA[i][j] || inputB[i][j];
            }
        }
        return result;
    }

    public static boolean[][] product(boolean[][] inputA, boolean[][] inputB) {
        boolean[][] result = Util.createArray(inputA);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = inputA[i][j] && inputB[i][j];
            }
        }
        return result;
    }

    public static boolean[][] subtraction(boolean[][] inputA, boolean[][] inputB) {
        boolean[][] result = Util.createArray(inputA);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = inputA[i][j] && !inputB[i][j];
            }
        }
        return result;
    }

    public static Item[][] invertMask(Item[][] input) {
        Item[][] result = input.clone();
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                if (result[i][j] == Item.BACKGROUND) {
                    result[i][j] = Item.FOREGROUND;
                } else if (result[i][j] == Item.FOREGROUND) {
                    result[i][j] = Item.BACKGROUND;
                }
            }
        }
        return result;
    }

    public static BufferedImage copyImage(BufferedImage source) {
        BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics g = b.getGraphics();
        g.drawImage(source, 0, 0, null);
        g.dispose();
        return b;
    }
}
