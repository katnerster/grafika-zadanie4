package com.katner.grafika;

import java.awt.image.BufferedImage;

/**
 * Created by michal on 23.03.16.
 */
public class Subimage {
    public static BufferedImage getSubImage(BufferedImage source, int x, int y, int w, int h) {
        int leftUpperCornerX = x - (w / 2);
        int leftUpperCornerY = y - (h / 2);
        int rightBottonCornerX = x + (w / 2);
        int rightBottomCornerY = y + (h / 2);

        if (leftUpperCornerX >= 0
                && leftUpperCornerY >= 0
                && rightBottonCornerX < source.getWidth()
                && rightBottomCornerY < source.getHeight()) {
            return source.getSubimage(x, y, w, h);
        } else if (leftUpperCornerX < 0 || leftUpperCornerY < 0) {

        }
        return null;
    }
}
