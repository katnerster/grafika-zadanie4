package com.katner.grafika;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by michal on 04.05.16.
 */
public class BufferedImageArrayConverter {

    static boolean[][] toArray(BufferedImage source) {
        boolean[][] result = new boolean[source.getHeight()][];
        for (int i = 0; i < source.getHeight(); i++) {
            result[i] = new boolean[source.getWidth()];
        }
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = source.getRGB(j, i) == Color.BLACK.getRGB();
            }
        }
        return result;
    }

    static BufferedImage toImage(boolean[][] input) {
        BufferedImage result = new BufferedImage(input[0].length, input.length, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < result.getHeight(); i++) {
            for (int j = 0; j < result.getWidth(); j++) {
                if (input[i][j] == true) {
                    result.setRGB(j, i, Color.BLACK.getRGB());
                } else {
                    result.setRGB(j, i, Color.WHITE.getRGB());
                }
            }
        }
        return result;
    }
}
