package com.katner.grafika;

import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by michal on 03.04.16.
 */
public class BinarizationThresholdTest {
    private BufferedImage wheat;

    @Before
    public void setUp() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("wheatrust.jpg").getFile());
        wheat = ImageIO.read(file);
    }

    @Test
    public void percentBlackTest() {
        int percentBlack = PointOperations.percent(wheat, 50);
        int difference = Math.abs(percentBlack - 60);
        assert difference < 10;
    }

    @Test
    public void meanIterativeTest() {
        int meanIterative = PointOperations.meanIterative(wheat);
        int difference = Math.abs(meanIterative - 94);
        assert difference < 10;
    }

    @Test
    public void entropySelectionTest() {
        int entropySelection = PointOperations.entropySelection(wheat);
//        System.out.println(entropySelection);
        int difference = Math.abs(entropySelection - 66);
        assert difference < 10;
    }

    @Test
    public void minimumError() {
        int minimumError = PointOperations.minimumError(wheat);
        System.out.println(minimumError);
        int difference = Math.abs(minimumError - 24);
        assert difference < 10;
    }

    @Test
    public void fuzzyMinimumError() {
        int minimumError = PointOperations.fuzzyMinimumError(wheat);
        System.out.println(minimumError);
        int difference = Math.abs(minimumError - 54);
        assert difference < 10;
    }
}
