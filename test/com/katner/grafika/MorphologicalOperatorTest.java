package com.katner.grafika;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.TestCase;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author katnerm
 */
public class MorphologicalOperatorTest extends TestCase {

    public MorphologicalOperatorTest(String testName) {
        super(testName);
    }

    /**
     * Test of erode method, of class MorphologicalOperator.
     */
    public void testErode() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-erosion.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-erosion.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        Item[][] mask = new Item[][]{
                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
        };
        boolean[][] result = MorphologicalOperator.erode(input, mask);
        assertNotNull(result);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of dilate method, of class MorphologicalOperator.
     */
    public void testDilate() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-dilation.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-dilation.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        Item[][] mask = new Item[][]{
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
        };
        boolean[][] result = MorphologicalOperator.dilate(input, mask);
        assertNotNull(result);
        assertArrayEquals(expResult, result);
    }

    public void testOpen() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-opening.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-opening.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        Item[][] mask = new Item[][]{
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
        };
        boolean[][] result = MorphologicalOperator.open(input, mask);
        assertNotNull(result);
        assertArrayEquals(expResult, result);
    }

    public void testClose() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-closing.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-closing.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        Item[][] mask = new Item[][]{
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
        };
        boolean[][] result = MorphologicalOperator.close(input, mask);
        assertNotNull(result);
        assertArrayEquals(expResult, result);
    }

    public void testHitmissEdgeDetection() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-hitmiss-edge-detection.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-hitmiss-edge-detection.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));

        Item[][][] masks = new Item[][][]{
                {
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE}
                },
                {
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND}
                },
                {
                        {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                },
                {
                        {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                }

        };
        boolean[][] result = MorphologicalOperator.hitmiss(input, masks);
//        ImageIO.write(BufferedImageArrayConverter.toImage(result), "png", new File("/tmp/tmp.png"));

        assertNotNull(result);

        assertArrayEquals(expResult, result);
    }

    public void testThinning() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-thinning.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-thinning.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));

        Item[][][] masks = new Item[][][]{
                {
                        {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                },

                {
                        {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                },

                {
                        {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND}
                },

                {
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND}
                },

                {
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND}
                },

                {
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE}
                },


                {
                        {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND}
                },

                {
                        {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                }


        };
        boolean[][] result = MorphologicalOperator.thin(input, masks);
//        ImageIO.write(BufferedImageArrayConverter.toImage(result), "png", new File("/tmp/tmp-thin.png"));

        assertNotNull(result);

        assertArrayEquals(expResult, result);
    }

    public void testThickening() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input-thickening.bmp").getPath());

        boolean[][] input = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        file = new File(classLoader.getResource("output-thickening.bmp").getPath());
        boolean[][] expResult = BufferedImageArrayConverter.toArray(ImageIO.read(file));
        Item[][][] masks = new Item[][][]{
                {
                        {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND}
                },

                {
                        {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                },

                {
                        {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.FOREGROUND, Item.IGNORE, Item.BACKGROUND}
                },

                {
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.FOREGROUND, Item.FOREGROUND, Item.BACKGROUND},
                        {Item.IGNORE, Item.BACKGROUND, Item.BACKGROUND}
                },

                {
                        {Item.FOREGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.BACKGROUND, Item.BACKGROUND}
                },

                {
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE}
                },


                {
                        {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.BACKGROUND, Item.IGNORE, Item.FOREGROUND}
                },

                {
                        {Item.BACKGROUND, Item.BACKGROUND, Item.IGNORE},
                        {Item.BACKGROUND, Item.FOREGROUND, Item.FOREGROUND},
                        {Item.IGNORE, Item.FOREGROUND, Item.IGNORE}
                }
        };
        boolean[][] result = MorphologicalOperator.thick(input, masks);

        assertNotNull(result);

        assertArrayEquals(expResult, result);
    }
}
